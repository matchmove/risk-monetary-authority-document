package monetaryAuthorityDocuments

import (
	"strings"

	documents "bitbucket.org/matchmove/risk-document"
	"bitbucket.org/matchmove/risk-interests"
	"bitbucket.org/matchmove/utils"
)

const (
	// SourceMAS source location
	SourceMAS = "mas.gov.sg"
)

// MonetaryAuthorityDocument represent the file to be process
type MonetaryAuthorityDocument struct {
	documents.DocumentObject
	Reference string
	Interests interests.Interests
}

// NewMonetaryAuthorityDocument initilized MonetaryAuthorityDocument
func NewMonetaryAuthorityDocument(f documents.File) MonetaryAuthorityDocument {
	f.ReadFile()

	d := MonetaryAuthorityDocument{}
	d.Body = f.Contents
	d.Reference = f.Path + f.Filename
	return d
}

// Parse breaks d.Body into arrays
func (d *MonetaryAuthorityDocument) Parse() {
	slice := strings.Split(d.Body, "\n")
	for _, line := range slice {
		line = utils.TrimNewlineSpaces(line)
		if len(line) >= 5 {
			startLine := line[0:5]
			if strings.LastIndex(utils.TrimNewlineSpaces(startLine), ".")+1 == len(utils.TrimNewlineSpaces(startLine)) {
				lineSlice := utils.ArrayFilter(strings.Split(line[5:], "  "))

				if len(lineSlice) != 4 {
					continue
				}

				documents := []string{"passport:" + lineSlice[2]}
				d.Interests = append(d.Interests, interests.Interest{
					Source:           SourceMAS,
					Reference:        d.Reference,
					Name:             lineSlice[0],
					AliasGoodQuality: []string{lineSlice[0]},
					Type:             interests.InterestTypeIndividual,
					Address:          []string{lineSlice[3]},
					DOB:              []string{lineSlice[1]},
					Documents:        documents,
					Country:          []string{lineSlice[3]},
				})
			}
		}
	}
}

// Format parse the array in d.Entities and d.Individuals
// and assigns them to an array of Interest
func (d *MonetaryAuthorityDocument) Format() interests.Interests {
	return d.Interests
}
