package monetaryAuthorityDocuments

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	documents "bitbucket.org/matchmove/risk-document"
)

const (
	ProcessorSource = "documents/source/"
)

func TestNewMonetaryAuthorityDocument(t *testing.T) {
	files, err := documents.GetLatestFiles(ProcessorSource)
	if err != nil {
		t.Errorf("Exception occured in files.GetLatest: %s", err)
	}

	for _, file := range files {
		if strings.Contains(strings.ToLower(file.Filename), SourceMAS) {
			document := NewMonetaryAuthorityDocument(file)

			if "main.MonetaryAuthorityDocument" != fmt.Sprint(reflect.TypeOf(document)) {
				t.Errorf("NewGovUKDocument method expecting:%s, got:%s", "main.MonetaryAuthorityDocument", string(fmt.Sprint(reflect.TypeOf(document))))
			}

			document.Parse()
			interests := document.Format()
			if len(interests) == 0 {
				t.Errorf("document.Format is expected to have more than 1 item")
			}

		}
	}
}
